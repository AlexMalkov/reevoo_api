!SLIDE
# Reevoo API #

!SLIDE bullets incremental
# Where we are today #

* Our applications too complicated
* No single responsibility
* Revieworld DB access sharing
* Core changes - multiple apps changes

!SLIDE bullets incremental
# Naming convention #

* applications
  * reevoo\_name\_app.git
  * rv\_name\_app.git

* gems
  * reevoo\_name\_gem.git
  * rv\_name\_gem.git

!SLIDE bullets incremental
# Internal Apps has a prefix #

* a/name
* - easy to block from outside

!SLIDE
# Application #

routes

      namespace :api do
          scope ":locale", :locale => get_locales do
              match "status" => "status#show"
              scope ":format", :format => /html|json/ do
                  namespace :v1 do
                      resources :reviews do
                      end
                  end
                  namespace :v2 do
                      resources :reviewables do
                          member do
                            get :with_reviews
                          end
                      end
                  end
              end
          end
      end

      a/revieworld/api/en-GB/json/v1/reviewables/1234/with_reviews


!SLIDE
# Application #

* controllers

      api
          v1
              reviews_controller.rb
          v2
              reviews_controller.rb
          api_controller.rb

* adapters:

* models:


!SLIDE bullets incremental
# Finaly #

* Our future is in our hands

* ![future](causality.jpg)
